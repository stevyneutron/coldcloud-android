package network.revolutions.app.coldcloud.object;

import java.util.ArrayList;

import network.revolutions.app.coldcloud.MainActivity;
import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.params.AppParamBlogNotification;
import network.revolutions.app.coldcloud.params.AppParamDailyStats;
import network.revolutions.app.coldcloud.params.AppParamFeedback;
import network.revolutions.app.coldcloud.params.AppParamImageCompression;
import network.revolutions.app.coldcloud.params.AppParamLinks;
import network.revolutions.app.coldcloud.params.AppParamLocking;
import network.revolutions.app.coldcloud.params.AppParamLogs;
import network.revolutions.app.coldcloud.params.AppParamRememberAccount;
import network.revolutions.app.coldcloud.params.AppParamRememberZone;
import network.revolutions.app.coldcloud.params.AppParamSendCrash;
import network.revolutions.app.coldcloud.params.AppParamSyncChart;
import network.revolutions.app.coldcloud.params.AppParamTheme;
import network.revolutions.app.coldcloud.params.AppParamVersion;
import network.revolutions.app.coldcloud.params.ParamAddressObfuscation;
import network.revolutions.app.coldcloud.params.ParamAlwaysHTTPS;
import network.revolutions.app.coldcloud.params.ParamAlwaysOnline;
import network.revolutions.app.coldcloud.params.ParamAuthenticateOrigin;
import network.revolutions.app.coldcloud.params.ParamAutoMinify;
import network.revolutions.app.coldcloud.params.ParamBrotli;
import network.revolutions.app.coldcloud.params.ParamCacheTTL;
import network.revolutions.app.coldcloud.params.ParamCachingLevel;
import network.revolutions.app.coldcloud.params.ParamDevelopmentMode;
import network.revolutions.app.coldcloud.params.ParamEarlyHints;
import network.revolutions.app.coldcloud.params.ParamEdgeCertificates;
import network.revolutions.app.coldcloud.params.ParamEncryptionMode;
import network.revolutions.app.coldcloud.params.ParamHTTP2;
import network.revolutions.app.coldcloud.params.ParamHTTP3;
import network.revolutions.app.coldcloud.params.ParamHTTPSRewrites;
import network.revolutions.app.coldcloud.params.ParamHotlinkProtection;
import network.revolutions.app.coldcloud.params.ParamIPGeo;
import network.revolutions.app.coldcloud.params.ParamIPv6;
import network.revolutions.app.coldcloud.params.ParamMinimumTLS;
import network.revolutions.app.coldcloud.params.ParamMirage;
import network.revolutions.app.coldcloud.params.ParamOnionRouting;
import network.revolutions.app.coldcloud.params.ParamOpportunisticEncryption;
import network.revolutions.app.coldcloud.params.ParamOriginCertificates;
import network.revolutions.app.coldcloud.params.ParamPolish;
import network.revolutions.app.coldcloud.params.ParamPrivacyPass;
import network.revolutions.app.coldcloud.params.ParamPseudoIPv4;
import network.revolutions.app.coldcloud.params.ParamPurgeCache;
import network.revolutions.app.coldcloud.params.ParamRocketLoader;
import network.revolutions.app.coldcloud.params.ParamSSLRecommender;
import network.revolutions.app.coldcloud.params.ParamServersideExcludes;
import network.revolutions.app.coldcloud.params.ParamTLS13;
import network.revolutions.app.coldcloud.params.ParamWebP;
import network.revolutions.app.coldcloud.params.ParamWebSockets;
import network.revolutions.app.coldcloud.ui.LayoutManager;

public class Parameter {

    public static final int ZONE = 0;
    public static final int SSL_TLS = 1;
    public static final int CERTIFICATES = 2;
    public static final int NETWORK = 3;
    public static final int CACHING = 4;
    public static final int SPEED = 5;
    public static final int SCRAPE_SHIELD = 6;
    public static final int NOTIFICATIONS = 7;
    public static final int APP = 99;

    public static ArrayList<Integer> getCategories() {
        ArrayList<Integer> list = new ArrayList<>();

        if (LayoutManager.get(LayoutManager.ZONE_CONFIG)) {
            list.add(ZONE);
            list.add(SSL_TLS);
            list.add(NETWORK);
            list.add(CACHING);
            list.add(SPEED);
            list.add(SCRAPE_SHIELD);
        }
        if (LayoutManager.get(LayoutManager.CERTIFICATES)) list.add(CERTIFICATES);
        if (LayoutManager.get(LayoutManager.NOTIFICATIONS)) list.add(NOTIFICATIONS);

        list.add(APP);
        return list;
    }

    public static int getName(int category) {
        switch (category) {
            case ZONE: return R.string.zone;
            case SSL_TLS: return R.string.ssl_tls;
            case CERTIFICATES: return R.string.certificates;
            case NETWORK: return R.string.network;
            case CACHING: return R.string.caching;
            case SPEED:  return R.string.speed;
            case SCRAPE_SHIELD: return R.string.scrape_shield;
            case NOTIFICATIONS: return R.string.notifications;
            case APP: return R.string.coldcloud;
            default: return R.string.question;
        }
    }

    public static int getIcon(int category) {
        switch (category) {
            case ZONE: return R.drawable.ic_globe;
            case SSL_TLS: return R.drawable.ic_lock;
            case CERTIFICATES: return R.drawable.ic_certificate;
            case NETWORK: return R.drawable.ic_network;
            case CACHING: return R.drawable.ic_cache;
            case SPEED: return R.drawable.ic_speed;
            case SCRAPE_SHIELD: return R.drawable.ic_scrape_shield;
            case NOTIFICATIONS: return R.drawable.ic_bell;
            case APP: return R.drawable.ic_cog;
            default: return R.string.question;
        }
    }

    public static ArrayList<Param> getParams(int category, MainActivity main) {
        ArrayList<Param> list = new ArrayList<>();

        switch (category) {
            case ZONE:
                list.add(new ParamAlwaysOnline());
                list.add(new ParamMirage());
                list.add(new ParamPolish());
                list.add(new ParamWebP());
                list.add(new ParamPrivacyPass());
                list.add(new ParamHTTP2());
                list.add(new ParamHTTP3());
                return list;

            case SSL_TLS:
                list.add(new ParamEncryptionMode());
                list.add(new ParamAlwaysHTTPS());
                list.add(new ParamOpportunisticEncryption());
                list.add(new ParamTLS13());
                list.add(new ParamHTTPSRewrites());
                list.add(new ParamMinimumTLS());
                list.add(new ParamAuthenticateOrigin());
                list.add(new ParamSSLRecommender());
                return list;

            case CERTIFICATES:
                list.add(new ParamEdgeCertificates());
                list.add(new ParamOriginCertificates());
                return list;

            case NETWORK:
                list.add(new ParamIPv6());
                list.add(new ParamWebSockets());
                list.add(new ParamOnionRouting());
                list.add(new ParamPseudoIPv4());
                list.add(new ParamIPGeo());
                //list.add(new ParamMaximumUpload());
                return list;

            case CACHING:
                list.add(new ParamPurgeCache());
                list.add(new ParamCachingLevel());
                list.add(new ParamCacheTTL());
                list.add(new ParamAlwaysOnline());
                list.add(new ParamDevelopmentMode());
                return list;

            case SPEED:
                list.add(new ParamAutoMinify());
                list.add(new ParamBrotli());
                list.add(new ParamRocketLoader());
                //list.add(new ParamMobileRedirect());
                list.add(new ParamEarlyHints());
                return list;

            case SCRAPE_SHIELD:
                list.add(new ParamAddressObfuscation());
                list.add(new ParamServersideExcludes());
                list.add(new ParamHotlinkProtection());
                return list;

            case APP:
                list.add(new AppParamSyncChart());
                list.add(new AppParamRememberZone());
                list.add(new AppParamRememberAccount());
                list.add(new AppParamLocking().setActivity(main));
                list.add(new AppParamBlogNotification());
                list.add(new AppParamDailyStats());
                list.add(new AppParamTheme().setActivity(main));
                list.add(new AppParamLogs());
                list.add(new AppParamSendCrash());
                list.add(new AppParamImageCompression());
                //list.add(new AppParamCredit());
                list.add(new AppParamFeedback().setActivity(main));
                list.add(new AppParamLinks().setActivity(main));
                list.add(new AppParamVersion());
                return list;

            default:
                return list;
        }
    }
}
