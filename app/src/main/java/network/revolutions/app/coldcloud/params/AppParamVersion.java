package network.revolutions.app.coldcloud.params;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import network.revolutions.app.coldcloud.BuildConfig;
import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.object.Param;
import network.revolutions.app.coldcloud.object.Zone;

public class AppParamVersion extends Param {

    @Override
    public void onDraw(LayoutInflater inflater, LinearLayout parent, Zone zone) {
        View root = inflater.inflate(R.layout.param_version, parent, false);

        TextView version = root.findViewById(R.id.app_version);
        version.setText(BuildConfig.VERSION_NAME);

        parent.addView(root);
    }

}
