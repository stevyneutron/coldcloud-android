package network.revolutions.app.coldcloud.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.module.WorldMap;
import network.revolutions.app.coldcloud.object.CountryStat;

public class WorldTableFragment extends FragmentCC {

    private CountryStat stat;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup)  inflater.inflate(R.layout.scrollview, container, false);
        LinearLayout table = root.findViewById(R.id.scrollView_content);
        WorldMap.drawTable(stat, table, inflater, true);
        return root;
    }

    public void setStat(CountryStat stat) {
        this.stat = stat;
    }

    public boolean hasStat() {
        return !(stat == null);
    }
}
