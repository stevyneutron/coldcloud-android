package network.revolutions.app.coldcloud.tester;

import android.content.Context;

import java.util.ArrayList;

import network.revolutions.app.coldcloud.adapter.TokenTestAdapter;
import network.revolutions.app.coldcloud.network.CFApi;
import network.revolutions.app.coldcloud.object.FirewallRule;
import network.revolutions.app.coldcloud.ui.LayoutManager;

public class FirewallTester extends Tester {

    private static final String key = LayoutManager.FIREWALL;
    private static final String keyEdit = LayoutManager.FIREWALL_EDIT;

    public FirewallTester(Context context) {
        super(context);
        this.name = "Firewall";
        this.permission = "Zone.Zone - Zone.Firewall Services";
    }

    @Override
    public void runTest(int position, TokenTestAdapter adapter, String zone, TestListener listener) {
        super.runTest(position, adapter, zone, listener);
        setLoading(true);

        CFApi.getFirewallRules(context, zone, new CFApi.RuleListener() {
            @Override
            public void onResult(ArrayList<FirewallRule> rules) {
                FirewallTester.this.icon = SUCCESS;
                FirewallTester.this.result = "Can read firewall rules";
                setLoading(false);
                listener.onFinish(zone);
            }

            @Override
            public void onError(Exception e) {
                FirewallTester.this.icon = WARNING;
                FirewallTester.this.result = "Can't read firewall rules";
                setLayout(key, keyEdit, false);
                setLoading(false);
                listener.onFinish(zone);
            }
        });
    }

}
