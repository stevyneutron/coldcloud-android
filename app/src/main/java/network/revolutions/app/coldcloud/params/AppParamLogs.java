package network.revolutions.app.coldcloud.params;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.activity.LogsActivity;
import network.revolutions.app.coldcloud.object.Param;
import network.revolutions.app.coldcloud.object.Zone;

public class AppParamLogs extends Param {

    @Override
    public void onDraw(LayoutInflater inflater, LinearLayout parent, Zone zone) {
        View root = inflater.inflate(R.layout.param_logs, parent, false);
        super.onDraw(root, zone);

        ((TextView) root.findViewById(R.id.param_name)).setText(R.string.application_logs);
        ((TextView) root.findViewById(R.id.param_description)).setText(R.string.application_logs_description);
        root.setOnClickListener(view -> showLogs());

        parent.addView(root);
    }

    private void showLogs() {
        Intent intent = new Intent(context, LogsActivity.class);
        context.startActivity(intent);
    }
}
