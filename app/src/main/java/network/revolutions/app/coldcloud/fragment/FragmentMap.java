package network.revolutions.app.coldcloud.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.material.card.MaterialCardView;
import com.google.maps.android.data.Feature;
import com.google.maps.android.data.Layer;
import com.google.maps.android.data.geojson.GeoJsonFeature;
import com.google.maps.android.data.geojson.GeoJsonLayer;
import com.google.maps.android.data.geojson.GeoJsonPolygonStyle;

import io.sentry.Sentry;
import network.revolutions.app.coldcloud.R;
import network.revolutions.app.coldcloud.object.CountryStat;

public class FragmentMap extends FragmentCC implements OnMapReadyCallback, Layer.OnFeatureClickListener, GoogleMap.OnMapClickListener {

    private MapView mapView;
    private CountryStat stat = null;

    private MaterialCardView countryLayout;
    private TextView countryName;
    private TextView countryRequests;
    private TextView countryThreats;
    private TextView countryPercentage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = (ViewGroup)  inflater.inflate(R.layout.fragment_map, container, false);

        countryLayout = root.findViewById(R.id.country_layout);
        countryRequests = root.findViewById(R.id.country_requests);
        countryThreats = root.findViewById(R.id.country_threats);
        countryName = root.findViewById(R.id.country_name);
        countryPercentage = root.findViewById(R.id.country_percentage);
        countryLayout.setOnClickListener(view -> hideCountryInfo());
        hideCountryInfo();

        mapView = root.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return root;
    }

    @Override
    public void onMapReady(@NonNull GoogleMap map) {
        if (stat == null) {
            Toast.makeText(requireContext(), R.string.error_stat_null, Toast.LENGTH_SHORT).show();
            setLoading(false);
            return;
        }

        try {
            // style the map
            map.setMapStyle(MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style));
            // create layers
            createLayers(map);
            // add listener
            map.setOnMapClickListener(this);
            // resume it = run
            mapView.onResume();
            setLoading(false);
        } catch (Exception e) {
            setLoading(false);
            e.printStackTrace();
            Sentry.captureException(e);
            Toast.makeText(requireContext(), "Error loading GeoJSON", Toast.LENGTH_SHORT).show();
        }
    }

    private void createLayers(GoogleMap map) throws Exception {
        GeoJsonLayer layer = new GeoJsonLayer(map, R.raw.countries, requireContext());
        for (GeoJsonFeature feature : layer.getFeatures()) {
            // get country
            String iso = feature.getProperty("iso");
            CountryStat.Country country = stat.getByISO(iso);
            // style feature
            styleFeature(feature, country);
        }
        layer.setOnFeatureClickListener(this);
        layer.addLayerToMap();
    }

    private void styleFeature(GeoJsonFeature feature, CountryStat.Country country) {
        // create style
        GeoJsonPolygonStyle style = new GeoJsonPolygonStyle();
        style.setStrokeWidth(0);

        // if country null continue
        if (country == null) {
            feature.setPolygonStyle(style);
            return;
        }

        // color country
        style.setPolygonFillColor(getColor(country.requests));
        feature.setPolygonStyle(style);
    }

    private int getColor(int requests) {
        float percentage = Math.round(getPercentage(requests));
        int p = Math.round( ((100-percentage) / 100) * 200);
        return Color.rgb(255, p, 0);
    }

    @Override
    public void onFeatureClick(Feature feature) {
        CountryStat.Country c = stat.getByISO(feature.getProperty("iso"));
        countryName.setText(feature.getProperty("name"));
        countryRequests.setText(c == null ? getString(R.string.null_or_zero) : String.valueOf(c.requests));
        countryThreats.setText(c == null ? getString(R.string.null_or_zero) : String.valueOf(c.threats));
        countryPercentage.setText(c == null ? "0%" : getPercentage(c.requests) + " %");
        showCountryInfo();
    }

    @Override
    public void onMapClick(@NonNull LatLng latLng) {
        hideCountryInfo();
    }

    public float getPercentage(int r) {
        return ((r / stat.total) * 100);
    }

    public boolean hasStat() { return !(stat == null); }

    public void setStat(CountryStat stat) { this.stat = stat; }

    private void showCountryInfo() { countryLayout.setVisibility(View.VISIBLE); }

    private void hideCountryInfo() { countryLayout.setVisibility(View.GONE); }
}
