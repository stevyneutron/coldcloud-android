package network.revolutions.app.coldcloud;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import io.sentry.Sentry;
import network.revolutions.app.coldcloud.object.Logger;
import network.revolutions.app.coldcloud.ui.LayoutManager;
import network.revolutions.app.coldcloud.work.ImageManager;

public class ColdCloud extends Application {

    @SuppressLint("DefaultLocale")
    @Override
    public void onCreate() {
        super.onCreate();
        initCrashHandler();
        Logger.init();
        Logger.info(String.format("Coldcloud %s [%d] Starting ...", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE));
        ImageManager.start(this);
        LayoutManager.init(this);
    }

    private void initCrashHandler() {
        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
            Log.d("MainThread", "uncaughtException: APPLICATION CRASH !");
            Sentry.captureException(throwable);
            throwable.printStackTrace();
            System.exit(1);
        });
    }

}
